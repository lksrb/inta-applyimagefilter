@group(0) @binding(0) 
var<storage, read_write> imageData: array<u32>;

@group(0) @binding(1) 
var<storage, read_write> textureSize: vec2<u32>;

fn getIndex(x: u32, y: u32) -> u32 
{
	return x * textureSize.y + y;
}

@compute @workgroup_size(1) 
fn main(@builtin(global_invocation_id) id: vec3<u32>) 
{
    let currentIndex = getIndex(id.x, id.y);
    let imagePixelData = imageData[currentIndex];
	
	// Process and return result
	var result = vec4<f32>(0.0);
	{
		let blockSize = 32u;
		let startPos = id.xy * blockSize;

		let blockWidth = min(blockSize, textureSize.y - startPos.x);
		let blockHeight = min(blockSize, textureSize.x - startPos.y);

		let numPixels = 1/f32(blockWidth * blockHeight);

		for (var i: u32 = 0; i < blockWidth; i++)
		{
		    for (var j: u32 = 0; j < blockHeight; j++)
		    {
				let index = getIndex(startPos.x + i, startPos.y + j);
				result += unpack4x8unorm(imageData[index]);
		    }
		}

		result *= numPixels;
		
		// Do not change opacity.
		result.w = 1.0;
		
		// Apply changes
		for (var i: u32 = 0; i < blockWidth; i++)
		{
		    for (var j: u32 = 0; j < blockHeight; j++)
		    {
				let index = getIndex(startPos.x + i, startPos.y + j);
				imageData[index] = pack4x8unorm(result);
		    }
		}
	}
}