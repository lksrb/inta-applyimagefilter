@group(0) @binding(0) 
var<storage, read_write> imageData: array<u32>;

@group(0) @binding(1) 
var<storage, read_write> textureSize: vec2<u32>;

@compute @workgroup_size(1) 
fn main(@builtin(global_invocation_id) id: vec3<u32>) {
    let currentIndex = id.x * textureSize.y + id.y;
    let imagePixelData = imageData[currentIndex];
    var color = unpack4x8unorm(imagePixelData);

    // Reduce blue 
    color.z *= 0.5;

    imageData[currentIndex] = pack4x8unorm(color);
}