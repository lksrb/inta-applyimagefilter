@echo off

REM Check if EMSDK is setup. If not, then set it
if "%EMSDK%" == "" (
    call emsdk activate latest
)

REM Emscripten compiler has issues with build incrementing
rm AIF.o

call make
echo Finished building.
