// Standard headers
#include <string>
#include <list>
#include <cstdint>
#include <cstdio>
#include <chrono>

// Third party headers
#define IMGUI_ENABLE_FREETYPE
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui.h>
#include <imgui_internal.h>
#include <imgui_stdlib.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_wgpu.h>

#include <webgpu/webgpu.h>
#include <GLFW/glfw3.h>

#include <emscripten.h>
#include <emscripten/html5.h>
#include <emscripten/html5_webgpu.h>

#include <emscripten_browser_file.h>
#include <emscripten_browser_clipboard.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

// Project headers
#include "Source/PrimitiveTypes.h"
#include "Source/Log.h"
#include "Source/Buffer.h"

// Project sources
#include "Source/FileSystem.cpp"
#include "Source/WGPUContext.cpp"
#include "Source/Texture.cpp"
#include "Source/UIStyles.cpp"
#include "Source/UI.cpp"
#include "Source/Filter.cpp"
#include "Source/ApplyImageFilter.cpp"

namespace AIF {

	static void Init()
	{
		// Initialize WebGPU environment
		WGPUContext::Init();

		// Initialize GLFW, ImGui and WGPU swapchain
		UI::Init();

		// Initialize our application
		ApplyImageFilter::Init();
	}

	static void Run()
	{
		emscripten_set_main_loop([]()
			{
				// Start ImGui frame, resize swapchain if necessary
				UI::Get()->Begin();

				// Update our application
				ApplyImageFilter::Get()->OnUpdate();

				// End imgui frame and render
				UI::Get()->End();
			}, 0, true);
	}

	static void Shutdown()
	{
		ApplyImageFilter::Shutdown();
		UI::Shutdown();
		WGPUContext::Shutdown();
	}

}

// EntryPoint
int main(int argc, char** argv)
{
	AIF_LOG("Hello from C++!");

	AIF::Init();
	AIF::Run();
	AIF::Shutdown();

	return 0;
}
