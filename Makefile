#
# Apply image filter makefile
#

CXX = em++
WEB_DIR = .
EXE = $(WEB_DIR)/index.js
IMGUI_DIR = Dependencies/imgui
STB_DIR = Dependencies/stb
EBC_DIR = Dependencies/emscripten_browser_clipboard
EBF_DIR = Dependencies/emscripten_browser_file

SOURCES = AIF.cpp
SOURCES += $(IMGUI_DIR)/imgui.cpp $(IMGUI_DIR)/imgui_demo.cpp $(IMGUI_DIR)/imgui_draw.cpp $(IMGUI_DIR)/imgui_tables.cpp $(IMGUI_DIR)/imgui_widgets.cpp $(IMGUI_DIR)/imgui_stdlib.cpp
SOURCES += $(IMGUI_DIR)/imgui_impl_glfw.cpp $(IMGUI_DIR)/imgui_impl_wgpu.cpp
OBJS = $(addsuffix .o, $(basename $(notdir $(SOURCES))))
CPPFLAGS =
LDFLAGS =
EMS =

##---------------------------------------------------------------------
## EMSCRIPTEN OPTIONS
##---------------------------------------------------------------------

# ("EMS" options gets added to both CPPFLAGS and LDFLAGS, whereas some options are for linker only)
EMS += -s DISABLE_EXCEPTION_CATCHING=1
LDFLAGS += -s USE_GLFW=3 -s USE_WEBGPU=1
LDFLAGS += -s WASM=1 -s ALLOW_MEMORY_GROWTH=1 -s NO_EXIT_RUNTIME=0 -s ASSERTIONS=1 -s FETCH=1

# Preload resouces
LDFLAGS += --preload-file Resources

# Enable exporting C functions
LDFLAGS += -s EXPORTED_RUNTIME_METHODS=[ccall]
LDFLAGS += -s EXPORTED_FUNCTIONS=[_main,_malloc,_free]

## File system settings
LDFLAGS += --no-heap-copy

##---------------------------------------------------------------------
## FINAL BUILD FLAGS
##---------------------------------------------------------------------

CPPFLAGS += -I$(IMGUI_DIR) -I$(IMGUI_DIR)/backends
CPPFLAGS += -I$(STB_DIR)
CPPFLAGS += -I$(EBC_DIR)
CPPFLAGS += -I$(EBF_DIR)

CPPFLAGS += -Wall -Wformat $(EMS)
CPPFLAGS += -std=c++20

# Optimize for speed
CPPFLAGS += -Ofast

# Fast build
#CPPFLAGS += -O0

LDFLAGS += $(EMS)

##---------------------------------------------------------------------
## BUILD RULES
##---------------------------------------------------------------------

%.o:%.cpp
	$(CXX) $(CPPFLAGS) -c -o $@ $<

%.o:$(IMGUI_DIR)/%.cpp
	$(CXX) $(CPPFLAGS) -c -o $@ $<

%.o:$(IMGUI_DIR)/%.cpp
	$(CXX) $(CPPFLAGS) -c -o $@ $<

$(EXE): $(OBJS) $(WEB_DIR)
	$(CXX) -o $@ $(OBJS) $(LDFLAGS)

clean:
	rm -f $(EXE) $(OBJS) $(WEB_DIR)/index.js $(WEB_DIR)/index.wasm $(WEB_DIR)/*.wasm.pre
