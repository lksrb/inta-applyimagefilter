#include "WGPUContext.h"

namespace AIF {

	WGPUContext::WGPUContext()
	{
		m_Instance = wgpuCreateInstance(nullptr);
		m_Device = emscripten_webgpu_get_device();

		WGPUSurfaceDescriptorFromCanvasHTMLSelector canvDesc = {};
		canvDesc.chain.sType = WGPUSType_SurfaceDescriptorFromCanvasHTMLSelector;
		canvDesc.selector = "#canvas";

		WGPUSurfaceDescriptor surfDesc = {};
		surfDesc.nextInChain = reinterpret_cast<WGPUChainedStruct*>(&canvDesc);
		m_Surface = wgpuInstanceCreateSurface(m_Instance, &surfDesc);

		WGPUAdapter adapter = {};
		m_PreferredFormat = static_cast<WGPUTextureFormat>(wgpuSurfaceGetPreferredFormat(m_Surface, adapter));

		wgpuDeviceSetUncapturedErrorCallback(m_Device, [](WGPUErrorType errorType, const char* message, void*)
			{
				const char* errorTypeLabel = "";
				switch (errorType)
				{
				case WGPUErrorType_Validation:
					errorTypeLabel = "Validation";
					break;
				case WGPUErrorType_OutOfMemory:
					errorTypeLabel = "Out of memory";
					break;
				case WGPUErrorType_Unknown:
					errorTypeLabel = "Unknown";
					break;
				case WGPUErrorType_DeviceLost:
					errorTypeLabel = "Device lost";
					break;
				default:
					errorTypeLabel = "Unknown";
				}

				AIF_LOG("[%s] error: %s", errorTypeLabel, message);
			}, nullptr);

		m_Queue = wgpuDeviceGetQueue(m_Device);

		AIF_LOG("WGPUContext initialized.");
	}

	WGPUContext::~WGPUContext()
	{
		wgpuSurfaceRelease(m_Surface);
		wgpuInstanceRelease(m_Instance);

		AIF_LOG("WGPUContext shutdown.");
	}

}