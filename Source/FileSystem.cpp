#include "FileSystem.h"

namespace AIF {
	
	Buffer FileSystem::ReadText(const char* filename)
	{
		FILE* file = fopen(filename, "rb");
		if (file == nullptr)
		{
			AIF_LOG("Error opening file");
			return Buffer();
		}

		// Seek to the end
		fseek(file, 0, SEEK_END);
		long fileSize = ftell(file);
		rewind(file);

		// Read the file
		Buffer buffer(fileSize + 1);
		fread(buffer.As<char>(), 1, fileSize, file);

		// Null-terminate the string
		buffer[fileSize] = '\0';

		// Close the file
		fclose(file);

		return buffer;
	}

	Buffer FileSystem::ReadBinary(const char* filename)
	{
		FILE* file = fopen(filename, "rb");
		if (file == nullptr)
		{
			AIF_LOG("Error opening file");
			return Buffer();
		}

		// Seek to the end
		fseek(file, 0, SEEK_END);
		long fileSize = ftell(file);
		rewind(file);

		// Read the file
		Buffer buffer(fileSize);
		fread(buffer.As<char>(), 1, fileSize, file);

		// Close the file
		fclose(file);

		return buffer;
	}

}