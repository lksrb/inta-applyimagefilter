#include "Texture.h"

namespace AIF {

	namespace Utils {

		u32 GetBytesCountFromFormat(WGPUTextureFormat format)
		{
			return 4;
		}

	}

	Texture::Texture(const TextureConfig& config)
		: m_Config(config), m_BytesFormat(Utils::GetBytesCountFromFormat(m_Config.Format))
	{
	}

	Texture::~Texture()
	{
		wgpuTextureViewRelease(m_ViewHandle);
		wgpuTextureDestroy(m_Handle);
	}

	void Texture::Invalidate(const stbi_uc* imageData, u32 width, u32 height)
	{
		auto device = WGPUContext::Get()->GetDevice();

		m_Width = width;
		m_Height = height;

		if (m_Handle)
		{
			wgpuTextureViewRelease(m_ViewHandle);
			wgpuTextureDestroy(m_Handle);
		}

		// Create texture
		WGPUTextureDescriptor textureDescriptor = {};
		textureDescriptor.dimension = WGPUTextureDimension_2D;
		textureDescriptor.size = { m_Width, m_Height, 1 };
		textureDescriptor.format = m_Config.Format;
		textureDescriptor.mipLevelCount = 1;
		textureDescriptor.sampleCount = 1;
		textureDescriptor.usage = m_Config.Usage;
		m_Handle = wgpuDeviceCreateTexture(device, &textureDescriptor);

		// Create texture view
		WGPUTextureViewDescriptor viewDescriptor = {};
		viewDescriptor.format = m_Config.Format;
		viewDescriptor.dimension = WGPUTextureViewDimension_2D;
		viewDescriptor.baseMipLevel = 0;
		viewDescriptor.mipLevelCount = 1;
		viewDescriptor.baseArrayLayer = 0;
		viewDescriptor.arrayLayerCount = 1;
		m_ViewHandle = wgpuTextureCreateView(m_Handle, &viewDescriptor);

		// Load image to texture
		WGPURenderPassColorAttachment colorAttachment = {};
		colorAttachment.depthSlice = WGPU_DEPTH_SLICE_UNDEFINED;
		colorAttachment.loadOp = WGPULoadOp_Clear;
		colorAttachment.storeOp = WGPUStoreOp_Store;
		colorAttachment.view = m_ViewHandle; // Use the texture view as the color attachment
		colorAttachment.clearValue = { 1.0f, 0.0f, 0.0f, 1.0f }; // Clear color

		WGPURenderPassDescriptor renderPassDescriptor = {};
		renderPassDescriptor.colorAttachmentCount = 1;
		renderPassDescriptor.colorAttachments = &colorAttachment;

		SetData(imageData, width * height * m_BytesFormat);

		AIF_LOG("Image loaded.");
	}

	void Texture::SetData(const void* imageData, u32 size)
	{
		auto queue = WGPUContext::Get()->GetQueue();

		const u32 textureSize = m_Width * m_Height * m_BytesFormat;
		if (textureSize != size)
		{
			AIF_LOG("Texture size mismatch.");
			return;
		}

		// Upload image data to texture
		WGPUImageCopyTexture imageCopyTexture = {
			.texture = m_Handle,
			.mipLevel = 0,
			.origin = {0, 0, 0}
		};
		WGPUTextureDataLayout dataLayout = {
			.offset = 0,
			.bytesPerRow = m_Width * m_BytesFormat,
			.rowsPerImage = m_Height
		};

		WGPUExtent3D extent;
		extent.width = m_Width;
		extent.height = m_Height;
		extent.depthOrArrayLayers = 1;

		// Write image data to texture
		wgpuQueueWriteTexture(queue, &imageCopyTexture, imageData, textureSize, &dataLayout, &extent);

	}

}