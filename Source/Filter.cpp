#include "Filter.h"

namespace AIF {

	Filter::Filter()
	{
	}

	Filter::~Filter()
	{
		wgpuShaderModuleRelease(m_ShaderModule);
		wgpuComputePipelineRelease(m_Pipeline);
	}

	void Filter::Create(const char* name, const char* shaderPath, WGPUPipelineLayout layout, bool readOnly)
	{
		strcpy(m_Name, name);
		m_ReadOnly = readOnly;

		auto shaderCode = FileSystem::ReadText(shaderPath);
		Invalidate(shaderCode.As<const char>(), layout);
		shaderCode.Release();
	}

	void Filter::Invalidate(const char* shaderCode, WGPUPipelineLayout layout)
	{
		auto device = WGPUContext::Get()->GetDevice();

		if (m_Pipeline)
		{
			wgpuShaderModuleRelease(m_ShaderModule);
			wgpuComputePipelineRelease(m_Pipeline);
		}

		// Shader module
		{
			m_ShaderCode = shaderCode; // Deep copy unfortunetely
			WGPUShaderModuleWGSLDescriptor wgslModuleDesc = {};
			wgslModuleDesc.chain.sType = WGPUSType_ShaderModuleWGSLDescriptor;
			wgslModuleDesc.code = m_ShaderCode.c_str();

			WGPUShaderModuleDescriptor shaderModuleDesc = {};
			shaderModuleDesc.nextInChain = reinterpret_cast<WGPUChainedStruct*>(&wgslModuleDesc);
			m_ShaderModule = wgpuDeviceCreateShaderModule(device, &shaderModuleDesc);

			AIF_LOG("Compiled and created a compute shader module.");
		}

		// Pipeline
		{
			// Create compute pipeline
			WGPUComputePipelineDescriptor pipelineDesc = {};
			pipelineDesc.compute.module = m_ShaderModule;
			pipelineDesc.compute.entryPoint = "main";
			pipelineDesc.layout = layout;
			m_Pipeline = wgpuDeviceCreateComputePipeline(device, &pipelineDesc);
		}
	}


}