namespace AIF {

	class ScopedTimer
	{
	public:
		ScopedTimer(const char* name, bool output = true)
			: m_Name(name), m_Output(output)
		{
			m_Start = std::chrono::high_resolution_clock::now();
		}

		f32 Stop()
		{
			m_End = std::chrono::high_resolution_clock::now();

			std::chrono::duration<f32> duration = m_End - m_Start;
			return duration.count() * 1000.0f;
		}

		~ScopedTimer()
		{
			if (m_Output == false)
				return;

			m_End = std::chrono::high_resolution_clock::now();

			std::chrono::duration<f32> duration = m_End - m_Start;
			AIF_LOG("%s took %.5f ms", m_Name, duration.count() * 1000.0f);
		}
	private:
		std::chrono::time_point<std::chrono::steady_clock> m_Start, m_End;
		const char* m_Name;
		bool m_Output;
	};

}
