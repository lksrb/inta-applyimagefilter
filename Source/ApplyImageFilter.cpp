#include "ApplyImageFilter.h"

namespace AIF {

	ApplyImageFilter::ApplyImageFilter()
	{
		// Create display texture
		TextureConfig config;
		config.Usage = WGPUTextureUsage_CopySrc | WGPUTextureUsage_CopyDst | WGPUTextureUsage_TextureBinding | WGPUTextureUsage_RenderAttachment;
		config.Format = WGPUTextureFormat_RGBA8Unorm;
		m_DisplayTextureImage = new Texture(config);

		// Display default image
		auto fileData = FileSystem::ReadBinary("Resources/TestImages/TestImage.jpg");
		LoadImageFromFile(fileData.Data, fileData.Size);
		fileData.Release();

		RecreatePipelineLayout();

		// Create pixelation filter
		{
			AddNewFilter("Blue-light filter", "Resources/Shaders/BlueLightShader.wgsl", true);
			AddNewFilter("Pixelation filter", "Resources/Shaders/PixelationShader.wgsl", true);
			AddNewFilter("Switch filter", "Resources/Shaders/SwitchChannelsShader.wgsl", true);

			AIF_LOG("Created pixelation filter successfully.");
		}
	}

	ApplyImageFilter::~ApplyImageFilter()
	{
		// Calls destructors
		m_Filters.clear();

		wgpuBufferRelease(m_ImageBuffer);
		wgpuBufferRelease(m_HostImageBuffer);
		wgpuBindGroupLayoutRelease(m_BindGroupLayout);
		wgpuBindGroupRelease(m_BindGroup);
		wgpuPipelineLayoutRelease(m_PipelineLayout);

		delete m_DisplayTextureImage;
		m_DisplayTextureImage = nullptr;

		stbi_image_free((stbi_uc*)m_BaseHostImageBufferData);
		m_BaseHostImageBufferData = nullptr;
	}

	void ApplyImageFilter::OnUpdate()
	{
		RenderUI();

		if (m_DoSaveLayout)
		{
			m_DoSaveLayout = false;

			AIF_LOG("Saved layout to clipboard.");
			ImGui::SetClipboardText(ImGui::SaveIniSettingsToMemory());
		}
	}

	void ApplyImageFilter::RenderUI()
	{
		const ImGuiIO& io = ImGui::GetIO();
		ImGuiStyle& style = ImGui::GetStyle();
		//f32 ts = io.DeltaTime;

		// Begin dockspace
		{
			ImGuiDockNodeFlags dockSpaceFlags = ImGuiDockNodeFlags_None;

			// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
			// because it would be confusing to have two docking targets within each others.
			ImGuiWindowFlags windowFlags = ImGuiWindowFlags_NoDocking;

			const ImGuiViewport* viewport = ImGui::GetMainViewport();
			ImGui::SetNextWindowPos(viewport->WorkPos);
			ImGui::SetNextWindowSize(viewport->WorkSize);
			ImGui::SetNextWindowViewport(viewport->ID);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			windowFlags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
			windowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

			// When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background
			// and handle the pass-thru hole, so we ask Begin() to not render a background.
			if (dockSpaceFlags & ImGuiDockNodeFlags_PassthruCentralNode)
				windowFlags |= ImGuiWindowFlags_NoBackground;

			// Important: note that we proceed even if Begin() returns false (aka window is collapsed).
			// This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
			// all active windows docked into it will lose their parent and become undocked.
			// We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
			// any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
			ImGui::Begin("DockSpace Demo", nullptr, windowFlags);
			ImGui::PopStyleVar();

			ImGui::PopStyleVar(2);

			// Submit the DockSpace
			f32 minWinSizeX = style.WindowMinSize.x;
			style.WindowMinSize.x = 370.0f;
			if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
			{
				ImGuiID dockspaceID = ImGui::GetID("VulkanAppDockspace");
				ImGui::DockSpace(dockspaceID, ImVec2(0.0f, 0.0f), dockSpaceFlags);
			}
			style.WindowMinSize.x = minWinSizeX;
		}

		// Viewport
		{
			ImGuiWindowClass windowClass;
			windowClass.DockNodeFlagsOverrideSet = ImGuiDockNodeFlags_NoTabBar;
			ImGui::SetNextWindowClass(&windowClass);
			ImGui::Begin("Image viewport", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);

			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);

			if (ImGui::IsKeyDown(ImGuiKey_LeftCtrl) || ImGui::IsKeyDown(ImGuiKey_RightCtrl))
			{
				if (io.MouseWheel > 0.0f)
				{
					m_ImageZoom += 0.09f;
				}

				if (io.MouseWheel < 0.0f)
				{
					m_ImageZoom -= 0.09f;
				}
			}

			m_ImageZoom = ImClamp(m_ImageZoom, 0.1f, 2.0f);

			ImVec2 size = ImGui::GetContentRegionAvail();

			ImVec2 imageSize = { static_cast<f32>(m_DisplayTextureImage->GetWidth()), static_cast<f32>(m_DisplayTextureImage->GetHeight()) };
			imageSize *= m_ImageZoom;

			ImGui::SetCursorPos((size - imageSize) * 0.5f);
			ImGui::Image(m_DisplayTextureImage->GetViewHandle(), imageSize);

			ImGui::PopStyleVar(2);
			ImGui::End();
		}

		// Menu bar
		if (ImGui::BeginMainMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				if (ImGui::MenuItem("Open Image..."))
				{
					OpenImage();
				}

				if (ImGui::MenuItem("Save Image..."))
				{
					SaveImage();
				}

				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("Debug"))
			{
				if (ImGui::MenuItem("Show debug window"))
				{
					m_ShowDebugWindow = true;
				}

				ImGui::EndMenu();
			}

			ImGui::EndMainMenuBar();
		}

		// Filter search list 
		{
			ImGui::Begin("Filters");

			if (ImGui::Button("Create new filter"))
			{
				m_CreateNewFilterPopup = true;
			}

			ImGui::SameLine();

			if (ImGui::Button("Clear image"))
			{
				ClearFilters();
			}

			ImGui::Separator();
			ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 7);

			u32 buttonWidth = 0;
			const u32 widgetID = 712943729; // Unique identificator for these widgets
			u32 i = 0;
			for (auto& filter : m_Filters)
			{
				ImGui::PushID(widgetID + i++);

				// View shader code
				if (ImGui::Button(filter.GetName()))
				{
					m_CurrentFilterInEdit = &filter;
					m_ShaderTextEditorContent = filter.GetShaderCode();
					m_ReadOnlyShaderCode = filter.IsReadOnly();
				}

				buttonWidth = ImGui::GetItemRectSize().x;

				//auto contentAvail = ImGui::GetContentRegionAvail();
				ImGui::SameLine();

				ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.1f, 0.65f, 0.3f, 1.0f));
				if (ImGui::Button("Apply"))
				{
					ApplyFilter(filter);
				}
				ImGui::PopStyleColor();

				ImGui::PopID();

				ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 7);

				++i;
			}

			if (m_CreateNewFilterPopup)
			{
				if (!m_SetKeyboardFocus)
				{
					m_SetKeyboardFocus = true;
					ImGui::SetKeyboardFocusHere();
				}
				ImGui::PushItemWidth(buttonWidth);
				ImGui::InputText("##CreateNewFilterInputText", m_CreateNewFilterText, AIF_FILTER_NAME_LENGTH, ImGuiInputTextFlags_None);
				if (ImGui::IsKeyPressed(ImGuiKey_Enter) && m_CreateNewFilterText[0] != '\0')
				{
					AIF_LOG("%s", m_CreateNewFilterText);
					AddNewFilter(m_CreateNewFilterText, "Resources/Shaders/DummyShader.wgsl", false);

					// Auto view
					m_CurrentFilterInEdit = &m_Filters.back();
					m_ShaderTextEditorContent = m_CurrentFilterInEdit->GetShaderCode();
					m_ReadOnlyShaderCode = m_CurrentFilterInEdit->IsReadOnly();

					m_SetKeyboardFocus = false;
					m_CreateNewFilterPopup = false;
					m_CreateNewFilterText[0] = '\0';
				}

				if (ImGui::IsItemDeactivated())
				{
					AIF_LOG("LOST FOCUS");
					m_SetKeyboardFocus = false;
					m_CreateNewFilterPopup = false;
					m_CreateNewFilterText[0] = '\0';
				}

				ImGui::PopItemWidth();

			}

			ImGui::End();
		}

		// Bottom scale utilities
		{
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);

			ImGuiWindowClass windowClass;
			windowClass.DockNodeFlagsOverrideSet = ImGuiDockNodeFlags_NoTabBar | ImGuiDockNodeFlags_NoDockingOverMe;
			ImGui::SetNextWindowClass(&windowClass);
			ImGui::Begin("##ScaleUtilitiesWindow", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoResize);
			ImVec2 size = ImGui::GetContentRegionAvail();

			// Scale slider
			{
				f32 sliderHeight = ImGui::CalcTextSize("##Scale").y + style.FramePadding.x * 2.0f;
				f32 sliderWidth = size.x * 0.33f;
				ImGui::SetCursorPosX((size.x - sliderWidth) * 0.9f);
				ImGui::SetCursorPosY((size.y - sliderHeight) * 0.5f);
				ImGui::PushItemWidth(sliderWidth);
				ImGui::SliderFloat("##Scale", &m_ImageZoom, 0.1f, 2.0f, "", ImGuiSliderFlags_NoInput);
				ImGui::PopItemWidth();
			}

			ImGui::SameLine();
			ImGui::Text("%d%%", static_cast<u32>(m_ImageZoom * 100.0f));
			ImGui::End();
			ImGui::PopStyleVar(2);
		}

		// Shader text editor
		{
			ImGui::Begin("Shader Text Editor");

			ImGui::PushID(91265630);
			ImGui::BeginDisabled(m_CurrentFilterInEdit ? m_CurrentFilterInEdit->IsReadOnly() : true);
			if (ImGui::Button("Compile"))
			{
				SaveModifiedFilter();
			}
			ImGui::EndDisabled();
			ImGui::PopID();

			ImGui::SameLine();

			ImGui::PushID(12740192);
			ImGui::BeginDisabled(!m_CurrentFilterInEdit);
			if (ImGui::Button("Save to Disk"))
			{
				std::string name = m_CurrentFilterInEdit->GetName();
				name += ".wgsl";
				emscripten_browser_file::download(name, "type/text", m_CurrentFilterInEdit->GetShaderCode());
			}
			ImGui::EndDisabled();
			ImGui::PopID();

			ImVec2 contentSize = ImGui::GetContentRegionAvail();
			ImGui::InputTextMultiline("##ShaderTextEditor", &m_ShaderTextEditorContent, contentSize, ImGuiInputTextFlags_AllowTabInput);
			ImGui::End();
		}

		// Debug window
		if (m_ShowDebugWindow)
		{
			ImGui::Begin("Debug", &m_ShowDebugWindow);

			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

			if (ImGui::Button("Save Layout"))
			{
				m_DoSaveLayout = true;
			}

			ImGui::End();
		}

		// End dockspace
		{
			ImGui::End();
		}
	}

	void ApplyImageFilter::LoadImageFromFile(const void* fileData, u64 size)
	{
		if (m_BaseHostImageBufferData)
		{
			stbi_image_free((stbi_uc*)m_BaseHostImageBufferData);
		}

		int width, heigth;
		m_BaseHostImageBufferData = stbi_load_from_memory((stbi_uc*)fileData, size, &width, &heigth, nullptr, 4);
		m_BaseHostImageBufferDataSize = width * heigth * 4;

		m_DisplayTextureImage->Invalidate(m_BaseHostImageBufferData, (u32)width, (u32)heigth);
	}

	void ApplyImageFilter::OpenImage()
	{
		emscripten_browser_file::upload(".png,.jpg,.jpeg", [](std::string const& filename, std::string const& type, std::string_view buffer, void*)
			{
				Get()->LoadNewImage(buffer.data(), buffer.size());
			});
	}

	void ApplyImageFilter::SaveImage()
	{
		wgpuBufferMapAsync(m_HostImageBuffer, WGPUMapMode_Read, 0, m_BaseHostImageBufferDataSize, [](WGPUBufferMapAsyncStatus status, void* userdata)
			{
				if (status == WGPUBufferMapAsyncStatus_Success)
				{
					auto aif = Get();

					const u32 width = aif->m_DisplayTextureImage->GetWidth();
					const u32 height = aif->m_DisplayTextureImage->GetHeight();

					const void* imageData = wgpuBufferGetConstMappedRange(aif->m_HostImageBuffer, 0, aif->m_BaseHostImageBufferDataSize);

					int fileDataSize;
					auto fileData = stbi_write_png_to_mem(reinterpret_cast<const unsigned char*>(imageData), 0, width, height, 4, &fileDataSize);

					emscripten_browser_file::download("TestImageFiltered.png", "type/image", fileData, fileDataSize);

					stbi_image_free(fileData);

					wgpuBufferUnmap(aif->m_HostImageBuffer);
				}
				else
				{
					AIF_LOG("Failed to map buffer.");
				}
			}, nullptr);
	}

	void ApplyImageFilter::LoadNewImage(const char* fileData, u64 size)
	{
		AIF_LOG("Loaded a new image!");
		LoadImageFromFile(fileData, size);
		RecreatePipelineLayout();

		for (auto& filter : m_Filters)
		{
			filter.Invalidate(filter.GetShaderCode(), m_PipelineLayout);
		}

	}

	void ApplyImageFilter::ApplyFilter(const Filter& filter)
	{
		m_FilterApplied = true;

		// Submit filter compute pass
		WGPUContext::Get()->Submit([this, &filter](WGPUCommandEncoder encoder)
			{
				WGPUComputePassEncoder computePass = wgpuCommandEncoderBeginComputePass(encoder, nullptr);

				wgpuComputePassEncoderSetBindGroup(computePass, 0, m_BindGroup, 0, nullptr);
				wgpuComputePassEncoderSetPipeline(computePass, filter.GetPipeline());

				wgpuComputePassEncoderDispatchWorkgroups(computePass, m_DisplayTextureImage->GetWidth(), m_DisplayTextureImage->GetHeight(), 1);
				wgpuComputePassEncoderEnd(computePass);
			});

		// Copy gpu buffer to host buffer
		WGPUContext::Get()->Submit([this](WGPUCommandEncoder encoder)
			{
				wgpuCommandEncoderCopyBufferToBuffer(encoder, m_ImageBuffer, 0, m_HostImageBuffer, 0, m_BaseHostImageBufferDataSize);
			});

		// Then map it and write it back
		wgpuBufferMapAsync(m_HostImageBuffer, WGPUMapMode_Read, 0, m_BaseHostImageBufferDataSize, [](WGPUBufferMapAsyncStatus status, void* userdata)
			{
				if (status == WGPUBufferMapAsyncStatus_Success)
				{
					auto aif = Get();
					const void* imageData = wgpuBufferGetConstMappedRange(aif->m_HostImageBuffer, 0, aif->m_BaseHostImageBufferDataSize);

					aif->m_DisplayTextureImage->SetData(imageData, aif->m_BaseHostImageBufferDataSize);

					AIF_LOG("Copied texture to host buffer.");

					wgpuBufferUnmap(aif->m_HostImageBuffer);
				}
				else
				{
					AIF_LOG("Failed to map buffer.");
				}
			}, nullptr);
	}

	void ApplyImageFilter::AddNewFilter(const char* name, const char* shaderPath, bool readOnly)
	{
		Filter& filter = m_Filters.emplace_back();
		filter.Create(name, shaderPath, m_PipelineLayout, readOnly);
	}

	void ApplyImageFilter::SaveModifiedFilter()
	{
		if (m_CurrentFilterInEdit == nullptr)
		{
			AIF_LOG("No filter is selected.");
			return;
		}

		auto& filter = *m_CurrentFilterInEdit;
		if (filter.IsReadOnly())
		{
			AIF_LOG("Filter is read only.");
			return;
		}

		// Recreate filter to apply changes to the shader
		filter.Invalidate(m_ShaderTextEditorContent.c_str(), m_PipelineLayout);
	}

	void ApplyImageFilter::ClearFilters()
	{
		if (!m_FilterApplied)
		{
			AIF_LOG("No filter applied.");
			return;
		}

		auto queue = WGPUContext::Get()->GetQueue();

		m_FilterApplied = false;

		// Overwrite gpu buffer with host base
		wgpuQueueWriteBuffer(queue, m_ImageBuffer, 0, m_BaseHostImageBufferData, m_BaseHostImageBufferDataSize);

		// Overwrite host buffer with host base
		wgpuQueueWriteBuffer(queue, m_HostImageBuffer, 0, m_BaseHostImageBufferData, m_BaseHostImageBufferDataSize);

		// Overwrite texture with host base
		m_DisplayTextureImage->SetData(m_BaseHostImageBufferData, m_BaseHostImageBufferDataSize);

		AIF_LOG("Texture reset.");
	}

	void ApplyImageFilter::RecreatePipelineLayout()
	{
		auto device = WGPUContext::Get()->GetDevice();
		auto queue = WGPUContext::Get()->GetQueue();

		if (m_ImageBuffer)
		{
			wgpuBufferRelease(m_ImageBuffer);
			wgpuBufferRelease(m_HostImageBuffer);
			wgpuBindGroupLayoutRelease(m_BindGroupLayout);
			wgpuBindGroupRelease(m_BindGroup);
			wgpuPipelineLayoutRelease(m_PipelineLayout);
		}

		// Global gpu buffer
		{
			WGPUBufferDescriptor imageBufferDesc = {};
			imageBufferDesc.size = m_BaseHostImageBufferDataSize;
			imageBufferDesc.usage = WGPUBufferUsage_CopyDst | WGPUBufferUsage_Storage | WGPUBufferUsage_CopySrc;
			m_ImageBuffer = wgpuDeviceCreateBuffer(device, &imageBufferDesc);
			AIF_LOG("Buffer size: %llu", imageBufferDesc.size);

			wgpuQueueWriteBuffer(queue, m_ImageBuffer, 0, m_BaseHostImageBufferData, imageBufferDesc.size);
		}

		// Global host buffer
		{
			WGPUBufferDescriptor imageBufferDesc = {};
			imageBufferDesc.size = m_BaseHostImageBufferDataSize;
			imageBufferDesc.usage = WGPUBufferUsage_CopyDst | WGPUBufferUsage_MapRead;
			m_HostImageBuffer = wgpuDeviceCreateBuffer(device, &imageBufferDesc);

			wgpuQueueWriteBuffer(queue, m_HostImageBuffer, 0, m_BaseHostImageBufferData, imageBufferDesc.size);
		}

		// Uniform buffers to store width and height
		{
			// Create constants buffer
			struct TextureSize
			{
				u32 Width;
				u32 Height;
			};

			TextureSize size;
			size.Width = m_DisplayTextureImage->GetWidth();
			size.Height = m_DisplayTextureImage->GetHeight();

			WGPUBufferDescriptor bufferDesc = {};
			bufferDesc.size = sizeof(TextureSize);
			bufferDesc.usage = WGPUBufferUsage_Storage | WGPUBufferUsage_CopyDst;
			m_TextureSizeUniformBuffer = wgpuDeviceCreateBuffer(device, &bufferDesc);

			wgpuQueueWriteBuffer(queue, m_TextureSizeUniformBuffer, 0, &size, bufferDesc.size);
		}

		AIF_LOG("Created uniform buffers.");

		// Create layout for pipelines since it wont change

		// Bind group layout
		{
			WGPUBufferBindingLayout bufferBindingLayout = {};
			bufferBindingLayout.hasDynamicOffset = false;
			bufferBindingLayout.type = WGPUBufferBindingType_Storage;

			WGPUBindGroupLayoutEntry bindGroupLayoutEntries[2] = {};
			bindGroupLayoutEntries[0].binding = 0;
			bindGroupLayoutEntries[0].visibility = WGPUShaderStage_Compute;
			bindGroupLayoutEntries[0].buffer = bufferBindingLayout;

			bindGroupLayoutEntries[1].binding = 1;
			bindGroupLayoutEntries[1].visibility = WGPUShaderStage_Compute;
			bindGroupLayoutEntries[1].buffer = bufferBindingLayout;

			WGPUBindGroupLayoutDescriptor bindGroupLayoutDesc = {};
			bindGroupLayoutDesc.entryCount = 2;
			bindGroupLayoutDesc.entries = bindGroupLayoutEntries;

			m_BindGroupLayout = wgpuDeviceCreateBindGroupLayout(device, &bindGroupLayoutDesc);
		}

		// Bind group
		{
			WGPUBindGroupEntry bindGroupEntries[2] = {};
			bindGroupEntries[0].binding = 0;
			bindGroupEntries[0].buffer = m_ImageBuffer;
			bindGroupEntries[0].size = wgpuBufferGetSize(m_ImageBuffer);

			bindGroupEntries[1].binding = 1;
			bindGroupEntries[1].buffer = m_TextureSizeUniformBuffer;
			bindGroupEntries[1].size = wgpuBufferGetSize(m_TextureSizeUniformBuffer);

			WGPUBindGroupDescriptor bindGroupDesc = {};
			bindGroupDesc.entries = bindGroupEntries;
			bindGroupDesc.entryCount = 2;
			bindGroupDesc.layout = m_BindGroupLayout;

			m_BindGroup = wgpuDeviceCreateBindGroup(device, &bindGroupDesc);
		}

		// Pipeline layout
		{
			WGPUPipelineLayoutDescriptor pipelineLayoutDesc = {};
			pipelineLayoutDesc.bindGroupLayouts = &m_BindGroupLayout;
			pipelineLayoutDesc.bindGroupLayoutCount = 1;
			m_PipelineLayout = wgpuDeviceCreatePipelineLayout(device, &pipelineLayoutDesc);
		}

		AIF_LOG("Recreated render resources.");

	}

}

