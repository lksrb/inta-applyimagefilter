#pragma once

namespace AIF {

	struct TextureConfig
	{
		WGPUTextureFormat Format;
		WGPUBufferUsageFlags Usage;
	};

	class Texture
	{
	public:
		Texture(const TextureConfig& config);
		~Texture();

		void Invalidate(const stbi_uc* imageData, u32 width, u32 height);
		void SetData(const void* imageData, u32 size);

		u32 GetBytesFormat() const { return m_BytesFormat; }
		u32 GetWidth() const { return m_Width; }
		u32 GetHeight() const { return m_Height; }
		WGPUTextureFormat GetFormat() const { return m_Config.Format; }
		WGPUTexture GetHandle() const { return m_Handle; }
		WGPUTextureView GetViewHandle() const { return m_ViewHandle; }
	private:
		TextureConfig m_Config;
		u32 m_BytesFormat;
		u32 m_Width;
		u32 m_Height;
		WGPUTexture m_Handle = nullptr;
		WGPUTextureView m_ViewHandle = nullptr;
	};

}