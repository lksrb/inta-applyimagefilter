#pragma once

#define AIF_FILTER_NAME_LENGTH 20

namespace AIF {

	class Filter
	{
	public:
		Filter();
		~Filter();

		void Create(const char* name, const char* shaderPath, WGPUPipelineLayout layout, bool readOnly);
		void Invalidate(const char* shaderCode, WGPUPipelineLayout layout);

		const char* GetShaderCode() const { return m_ShaderCode.c_str(); }
		bool IsReadOnly() const { return m_ReadOnly; }
		const char* GetName() const { return m_Name; }
		WGPUComputePipeline GetPipeline() const { return m_Pipeline; }
	private:
		char m_Name[AIF_FILTER_NAME_LENGTH] = { 0 };
		bool m_ReadOnly = false;
		std::string m_ShaderCode;
		WGPUShaderModule m_ShaderModule = nullptr;
		WGPUComputePipeline m_Pipeline = nullptr;
	};

}