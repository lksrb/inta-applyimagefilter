#pragma once

namespace AIF {

	class FileSystem
	{
	public:
		static Buffer ReadText(const char* filename);
		static Buffer ReadBinary(const char* filename);
	};

}