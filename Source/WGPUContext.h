#pragma once

namespace AIF {

	class WGPUContext
	{
	public:
		static void Init()
		{
			s_Instance = new WGPUContext;
		}

		static void Shutdown()
		{
			delete s_Instance;
			s_Instance = nullptr;
		}
	public:
		WGPUContext();
		~WGPUContext();

		template<typename Func>
		void Submit(Func&& func)
		{
			auto encoder = wgpuDeviceCreateCommandEncoder(m_Device, nullptr);
			func(encoder);
			WGPUCommandBuffer commandBuffer = wgpuCommandEncoderFinish(encoder, NULL);
			wgpuQueueSubmit(m_Queue, 1, &commandBuffer);
		}

		WGPUInstance GetInstance() const { return m_Instance; }
		WGPUDevice GetDevice() const { return m_Device; }
		WGPUSurface GetSurface() const { return m_Surface; }
		WGPUQueue GetQueue() const { return m_Queue; }

		WGPUTextureFormat GetPreferredFormat() const { return m_PreferredFormat; }

		static WGPUContext* Get() { return s_Instance; }
	private:
		WGPUInstance m_Instance;
		WGPUDevice m_Device;
		WGPUSurface m_Surface;
		WGPUQueue m_Queue;
		WGPUTextureFormat m_PreferredFormat;

		static inline WGPUContext* s_Instance = nullptr;
	};

}