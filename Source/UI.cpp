#include "UI.h"

namespace AIF {

	UI::UI()
	{
		auto device = WGPUContext::Get()->GetDevice();

		glfwSetErrorCallback([](int error, const char* description)
			{
				AIF_LOG("GLFW Error %d: %s", error, description);
			});

		if (!glfwInit())
			return;

		// Make sure GLFW does not initialize any graphics context.
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		m_Window = glfwCreateWindow(m_SwapChainWidth, m_SwapChainHeight, "Apply image filter window", nullptr, nullptr);
		if (m_Window == nullptr)
			return;

		glfwShowWindow(m_Window);

		// Setup Dear ImGui context
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO();
		io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
		io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking

		// Setup Dear ImGui style
		//ImGui::StyleColorsDark();
		// ImGui::StyleColorsLight();
		Spectrum::StyleColorsSpectrum();

		// Disable automatic layout saving/loading
		io.IniFilename = nullptr;

		ImGui::LoadIniSettingsFromDisk("Resources/imgui.ini");

		// Setup Platform/Renderer backends
		ImGui_ImplGlfw_InitForOther(m_Window, true);
		ImGui_ImplGlfw_InstallEmscriptenCanvasResizeCallback("#canvas");

		ImGui_ImplWGPU_InitInfo wgpuInitInfo;
		wgpuInitInfo.Device = device;
		wgpuInitInfo.NumFramesInFlight = 3;
		wgpuInitInfo.RenderTargetFormat = WGPUContext::Get()->GetPreferredFormat();
		wgpuInitInfo.DepthStencilFormat = WGPUTextureFormat_Undefined;
		ImGui_ImplWGPU_Init(&wgpuInitInfo);

		// Add font
		

		// Increase subpixel quality
		ImFontConfig fontConfig;
		fontConfig.OversampleH = 3;
		fontConfig.OversampleV = 3;
		io.Fonts->AddFontFromFileTTF("Resources/Fonts/Roboto-Medium.ttf", 20.0f, &fontConfig);

		// Handle clipboarding
		{
			io.SetClipboardTextFn = [](void*, const char* data)
				{
					UI::Get()->m_ClipBoardStorage = data;
					emscripten_browser_clipboard::copy(UI::Get()->m_ClipBoardStorage);
				};
			io.GetClipboardTextFn = [](void*)
				{
					return UI::Get()->m_ClipBoardStorage.c_str();
				};

			emscripten_browser_clipboard::paste([](std::string const& pasteData, void*)
				{
					/// Callback to handle clipboard paste from browser
					UI::Get()->m_ClipBoardStorage = std::move(pasteData);
				});
		}

		AIF_LOG("UI initialized.");

		InvalidateSwapChain(m_SwapChainWidth, m_SwapChainHeight);
	}

	UI::~UI()
	{
		ImGui_ImplWGPU_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();

		glfwDestroyWindow(m_Window);
		glfwTerminate();
	}

	void UI::Begin()
	{
		// Poll and handle events
		glfwPollEvents();

		// React to changes in screen size
		int width, height;
		glfwGetFramebufferSize(m_Window, &width, &height);
		if (width != m_SwapChainWidth || height != m_SwapChainHeight)
		{
			ImGui_ImplWGPU_InvalidateDeviceObjects();
			InvalidateSwapChain(width, height);
			ImGui_ImplWGPU_CreateDeviceObjects();
		}

		ImGui_ImplWGPU_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
	}

	void UI::End()
	{
		auto device = WGPUContext::Get()->GetDevice();
		auto queue = WGPUContext::Get()->GetQueue();

		ImVec4 clearColor = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

		// Render imgui
		ImGui::Render();

		WGPURenderPassColorAttachment colorAttachment = {};
		colorAttachment.depthSlice = WGPU_DEPTH_SLICE_UNDEFINED;
		colorAttachment.loadOp = WGPULoadOp_Clear;
		colorAttachment.storeOp = WGPUStoreOp_Store;
		colorAttachment.clearValue = { clearColor.x * clearColor.w, clearColor.y * clearColor.w, clearColor.z * clearColor.w, clearColor.w };
		colorAttachment.view = wgpuSwapChainGetCurrentTextureView(m_SwapChain);

		WGPURenderPassDescriptor renderPassDesc = {};
		renderPassDesc.colorAttachmentCount = 1;
		renderPassDesc.colorAttachments = &colorAttachment;
		renderPassDesc.depthStencilAttachment = nullptr;

		WGPUCommandEncoderDescriptor encoderDesc = {};
		WGPUCommandEncoder encoder = wgpuDeviceCreateCommandEncoder(device, &encoderDesc);

		WGPURenderPassEncoder pass = wgpuCommandEncoderBeginRenderPass(encoder, &renderPassDesc);

		ImGui_ImplWGPU_RenderDrawData(ImGui::GetDrawData(), pass);
		wgpuRenderPassEncoderEnd(pass);

		WGPUCommandBufferDescriptor cmdBufferDesc = {};
		WGPUCommandBuffer cmd_buffer = wgpuCommandEncoderFinish(encoder, &cmdBufferDesc);
		wgpuQueueSubmit(queue, 1, &cmd_buffer);

		wgpuTextureViewRelease(colorAttachment.view);
		wgpuRenderPassEncoderRelease(pass);
		wgpuCommandEncoderRelease(encoder);
		wgpuCommandBufferRelease(cmd_buffer);
	}

	void UI::InvalidateSwapChain(i32 width, i32 height)
	{
		auto device = WGPUContext::Get()->GetDevice();
		auto surface = WGPUContext::Get()->GetSurface();

		if (m_SwapChain)
			wgpuSwapChainRelease(m_SwapChain);
		m_SwapChainWidth = width;
		m_SwapChainHeight = height;
		WGPUSwapChainDescriptor swap_chain_desc = {};
		swap_chain_desc.usage = WGPUTextureUsage_RenderAttachment;
		swap_chain_desc.format = WGPUContext::Get()->GetPreferredFormat();
		swap_chain_desc.width = width;
		swap_chain_desc.height = height;
		swap_chain_desc.presentMode = WGPUPresentMode_Fifo;
		m_SwapChain = wgpuDeviceCreateSwapChain(device, surface, &swap_chain_desc);
	}

}