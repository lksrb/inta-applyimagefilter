#pragma once

namespace AIF {
	
	class UI
	{
	public:
		static void Init()
		{
			s_Instance = new UI;
		}

		static void Shutdown()
		{
			delete s_Instance;
			s_Instance = nullptr;
		}
	public:
		UI();
		~UI();

		void Begin();
		void End();

		GLFWwindow* GetWindow() const { return m_Window; }

		static UI* Get() { return s_Instance; }
	private:
		void InvalidateSwapChain(i32 width, i32 height);
	private:
		std::string m_ClipBoardStorage;
		WGPUSwapChain m_SwapChain;
		i32 m_SwapChainWidth = 1280;
		i32 m_SwapChainHeight = 720;
		GLFWwindow* m_Window;
		static inline UI* s_Instance = nullptr;
	};

}