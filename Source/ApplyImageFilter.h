#pragma once

namespace AIF {

	class ApplyImageFilter
	{
	public:
		static void Init()
		{
			s_Instance = new ApplyImageFilter;
		}

		static void Shutdown()
		{
			delete s_Instance;
			s_Instance = nullptr;
		}
	public:
		ApplyImageFilter();
		~ApplyImageFilter();

		void OnUpdate();

		static ApplyImageFilter* Get() { return s_Instance; }
	private:
		void LoadNewImage(const char* fileData, u64 size);
		void RenderUI();

		void ApplyFilter(const Filter& filter);
		void ClearFilters();
		void AddNewFilter(const char* name, const char* shaderPath, bool readOnly);
		void SaveModifiedFilter();

		void LoadImageFromFile(const void* fileData, u64 size);

		void OpenImage();
		void SaveImage();

		void RecreatePipelineLayout();
	private:
		std::list<Filter> m_Filters; // NOTE: Seems to prevent weird runtime bug; --no-heap-copy seems to cause the issue
		std::string m_ShaderTextEditorContent;
		Filter* m_CurrentFilterInEdit = nullptr;
		bool m_ReadOnlyShaderCode = false;

		char m_CreateNewFilterText[AIF_FILTER_NAME_LENGTH] = { 0 };
		bool m_SetKeyboardFocus = false;
		bool m_CreateNewFilterPopup = false;

		bool m_FilterApplied = false;
		bool m_ShowDebugWindow = false;

		WGPUBuffer m_ImageBuffer = nullptr;
		WGPUBuffer m_HostImageBuffer = nullptr;
		const stbi_uc* m_BaseHostImageBufferData = nullptr;
		u32 m_BaseHostImageBufferDataSize = 0;

		WGPUBuffer m_TextureSizeUniformBuffer = nullptr;

		WGPUBindGroupLayout m_BindGroupLayout = nullptr;
		WGPUBindGroup m_BindGroup = nullptr;
		WGPUPipelineLayout m_PipelineLayout = nullptr;

		Texture* m_DisplayTextureImage = nullptr;
		bool m_DoSaveLayout = false;
		f32 m_ImageZoom = 0.7f;

		static inline ApplyImageFilter* s_Instance = nullptr;
	};

}
