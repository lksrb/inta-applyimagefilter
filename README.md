# Apply Image Filter

A simple web application for applying filters to images.

## Specifications

AIF uses emscripten compiler to transpile C++ code into web assembly.

Rendering API: WebGPU

Languages: C++ 20, JavaScript, HTML, CSS

### Features
- Loading/saving modified images
- Creating new filters
- Saving filter shaders
- Combining filters
- Partial support for live shader editation

## Getting started
Using your favourite text editor is recommended. For best experience consider using Google Chrome. 

<ins>**1. Downloading the repository**</ins>

Start by cloning this repository using `git clone https://gitlab.com/lksrb/inta-applyimagefilter`.

<ins>**2. Setting up the project**</ins>

Run [start.bat](https://gitlab.com/lksrb/inta-applyimagefilter/-/blob/main/start.bat?ref_type=heads) to start a local HTTP server on port 8000. This script uses python.

## Usage
Start Google Chrome and enter `localhost:8000` to access the web application.

